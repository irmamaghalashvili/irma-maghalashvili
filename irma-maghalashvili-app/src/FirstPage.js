import React from 'react';
import './FirstPage.css';
// import { Link } from 'react-router-dom';

// import { NavLink } from 'react-router-dom';


function FirstPage() {
  return (
    <div className="app">
      <div className="background"></div>
      <div className="content">
        <div className='profile-picture'>
          {/* <img src={"./assets/Profile.jpeg"} alt="Profile" className="profile-picture" /> */}
        </div>
        
        <h1>Irma Maghalashvili</h1>
        <h2>Front-End Developer</h2>
        <p>Over the past two years, I have acquired valuable skills in web development, focusing on HTML, CSS, JavaScript, TypeScript, and more recently, React. While I have not yet had the opportunity to work professionally in this field, my
dedication, adaptability, and strong work ethic have prepared me to excel.I thrive on challenges and view each new project as an opportunity to learn, innovate, and push my boundaries. My
unwavering motivation drives me to continually improve and contribute to the success of any team I join. </p>
        <button className='btn'>
          {/* <Link to='./SecondPage'>Know </Link> */}
          {/* <NavLink to="/SecondPage">Know more</NavLink> */}
        <a href='/SocondPage' className="know-more" rel='linklink'>Know More</a>
        </button>

      </div>
    </div>
  );
}

export default FirstPage;
