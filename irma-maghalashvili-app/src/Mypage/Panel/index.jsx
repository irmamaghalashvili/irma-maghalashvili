import React from 'react';
import './Panel.css';
import { IoIosPerson, IoIosBook, IoIosBriefcase, IoIosHammer, IoIosArrowBack, IoIosGrid, IoIosContacts, IoIosStar } from 'react-icons/io';




export default function Panel({ collapsed, onCollapse }) {
	return (
		<>
			<div className={`panel ${collapsed ? 'collapsed' : ''}`}>
				<div>
				<ul>
				<li><IoIosPerson /> About Me</li>
				<li><IoIosBook /> Education</li>
				<li><IoIosBriefcase /> Experience</li>
				<li><IoIosHammer /> Skills</li>
				<li><IoIosGrid /> Portfolio</li>
				<li><IoIosContacts /> Contacts</li>
				<li><IoIosStar /> Feedbacks</li>
              </ul>
			  <ul className={`ul-content ${collapsed ? 'collapsed' : ''}`}>
                  <li><IoIosPerson /></li>
				  <li><IoIosBook /></li>
				  <li><IoIosBriefcase /></li>
				  <li><IoIosHammer /></li>
				  <li><IoIosGrid /></li>
				  <li><IoIosContacts /></li>
				  <li><IoIosStar /></li>
                  
              </ul>
				</div>
				<button onClick={onCollapse} className="collapse-btn">
					&#8801;
				</button>
				<button onClick={onCollapse} className="go-back-btn">
				<IoIosArrowBack />Go back
				</button>
			</div>
		</>
	);
}
