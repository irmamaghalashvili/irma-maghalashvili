import React from 'react';
import { FaPhone, FaEnvelope, FaTwitter, FaFacebook, FaSkype } from 'react-icons/fa';
import './Contact.css';

function Contact() {
  return (
    <div className="contact">
        <h3>Contact</h3>
      <ul>
        <li>
          <a href="tel:your-phone-number">
            <FaPhone className='iconColor' /> Phone
          </a>
        </li>
        <li>
          <a href="mailto:your-email@example.com">
            <FaEnvelope className='iconColor'/> Email
          </a>
        </li>
        <li>
          <a href="https://twitter.com/your-twitter" >
            <FaTwitter className='iconColor' /> Twitter
          </a>
        </li>
        <li>
          <a href="https://www.facebook.com/magalashviliirma">
            <FaFacebook className='iconColor' /> Facebook </a>
        </li>
        <li>
          <a href="skype:your-skype-id?chat">
            <FaSkype className='iconColor' /> Skype
          </a>
        </li>
      </ul>
    </div>
  );
}

export default Contact;
