import React from 'react';
import './Portfolio.css';
import feedbackData from '../data/feedback.json'; // Import the JSON data

function FeedbackComponent() {
  return (
    <footer className='feedbacks'>
      <h3>Feedback</h3>
      {feedbackData.map((feedback, index) => (
        <div key={index} className="feedback-item">
          <p>{feedback.feedback}</p>
          <div className="reporter">
            <img src={feedback.reporter.photoUrl} alt={feedback.reporter.name} />
            <div className="reporter-info">
              <p className="reporter-name">{feedback.reporter.name}</p>
              <a href={feedback.reporter.citeUrl} className="reporter-cite">
                {feedback.reporter.citeUrl}
              </a>
            </div>
          </div>
        </div>
      ))}
    </footer>
  );
}

export default FeedbackComponent;
