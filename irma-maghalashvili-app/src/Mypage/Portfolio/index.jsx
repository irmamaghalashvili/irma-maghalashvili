import React  from 'react';
import './Portfolio.css';
import FeedbackComponent from './feedback';
import Exprerience from './experience';
import Portf from './Portf';
import Contact from './Contact';


function SecondPage( {collapsed} ) {	
	return (
    <div className={`container ${collapsed ? 'collapsed' : null}`}>

        <header className='about-me'>
            <h3>About Me</h3>
            <p>Over the past two years, I have acquired valuable skills in web development, focusing on HTML, CSS, JavaScript,
TypeScript, and more recently, React. While I have not yet had the opportunity to work professionally in this field, my
dedication, adaptability, and strong work ethic have prepared me to excel. 
   I thrive on challenges and view each new project as an opportunity to learn, innovate, and push my boundaries. My
unwavering motivation drives me to continually improve and contribute to the success of any team I join. </p>
        </header>
        <div className='education'>
            <h3>Education</h3>
			<div className='eduxationDiv'>
				<span>2006 <span className='hug'></span> 2010</span>
				<div className='eduDiv'>
					<h5>Bachelor/Accounting - Tax Specialty/ Sukhishvili University, Gori </h5>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quaerat rerum nam tempore ipsa assumenda molestiae dicta excepturi illo ducimus. Quo similique, illo rerum temporibus totam ullam cumque neque eligendi iure.</p>
				</div>
			</div>	
			<div className='eduxationDiv'>
				<span>2022 <span className='hug'></span>2023</span>
				<div className='eduDiv'>
					<h5>Certificate/Front End Development/ Business and Technology University</h5>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, exercitationem. Pariatur, repellat cum non, quod eaque nihil ad eveniet unde similique tenetur quibusdam! Necessitatibus inventore magni obcaecati autem! Dignissimos, ipsum.</p>
				</div>
			</div>

        </div>
        
			<Exprerience collapsed={true} />
      

        <div className='skills'>
            <h3>Skills</h3>
				<div className='html skill'>HTML</div>
				<div className='css skill'>CSS</div>
				<div className='javascript skill'>JAVASCRIPT</div>
				<div className='react skill'>REACT</div>
				<div className='skillBackground'></div>
				{/* <img src="../../../public/scale.png" alt="scale"/> */}
        </div>

			<Portf />

            
            <Contact />
        
        
			<FeedbackComponent collapsed={true} />
        
		<span> 
		<img src={"../../assets/next.png"} alt="icon" className="upBtn" />
		 </span>
      </div>
  );
}

export default SecondPage;
