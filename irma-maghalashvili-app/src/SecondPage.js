import { Portfolio, Panel } from './Mypage';
import { useState } from 'react';
import './SecondPage.css';

export default function CapStone() {
	const [collapsed, setCollapsed] = useState(true);

	return (
		<div className="secondpage">
			<Panel collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)} />
			<Portfolio collapsed={collapsed} />
		</div>
	);
}